package com.example.mylocations

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.mylocations.data.Location
import com.example.mylocations.data.LocationRepository

/**
 * Created by Alex Torge on 1/25/19.
 */
class LocationListViewModel(private val locationRepository : LocationRepository) : ViewModel() {

    fun addLocation(location: Location) = locationRepository.insertLocation(location)

    fun getLocations() : LiveData<List<Location>> = locationRepository.getLocations()

}