package com.example.mylocations.data

import androidx.lifecycle.LiveData

/**
 * Created by Alex Torge on 1/25/19.
 */
class LocationRepository constructor(private val locationsDao: LocationDao) {

    fun getLocations(): LiveData<List<Location>> = locationsDao.getLocations()


    fun getLocationsByLabel(label: String): LiveData<List<Location>> = locationsDao.getLocationByLabel(label)


    fun getLocationsByLatLon(lat: Double, lon: Double): LiveData<List<Location>> =
        locationsDao.getLocationByLatLon(lat, lon)


    fun insertLocation(location: Location) = locationsDao.insert(location)

    fun deleteLocation(location: Location) = locationsDao.deleteLocation(location)

}