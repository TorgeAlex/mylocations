package com.example.mylocations.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

/**
 * Created by Alex Torge on 1/25/19.
 */
@Entity
data class Location(@PrimaryKey (autoGenerate = true) val id: Int, val label:String, val lat : Double, val lon : Double) : Serializable