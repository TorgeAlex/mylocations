package com.example.mylocations.data

import java.io.Serializable

/**
 * Created by Alex Torge on 1/25/19.
 */
data class LatLon(val lat: Double, val lon: Double) : Serializable {
    override fun toString(): String {
        return "LatLon(lat=$lat, lon=$lon)"
    }
}