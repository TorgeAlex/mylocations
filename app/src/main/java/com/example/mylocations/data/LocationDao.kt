package com.example.mylocations.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

/**
 * Created by Alex Torge on 1/25/19.
 */
@Dao
interface LocationDao {

    @Query("Select * FROM Location")
    fun getLocations(): LiveData<List<Location>>

    @Query("Select * from Location Where label = :label")
    fun getLocationByLabel(label: String): LiveData<List<Location>>

    @Query("Select * from Location Where lat = :lat and lon = :lon")
    fun getLocationByLatLon(lat: Double, lon : Double): LiveData<List<Location>>

    @Delete
    fun deleteLocation(location: Location)

    @Insert(onConflict = REPLACE)
    fun insert(location: Location)
}