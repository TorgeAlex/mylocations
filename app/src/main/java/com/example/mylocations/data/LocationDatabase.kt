package com.example.mylocations.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


/**
 * Created by Alex Torge on 1/25/19.
 */
@Database(version = 1, exportSchema = false, entities = [Location::class])
abstract class LocationDatabase : RoomDatabase() {

    abstract fun locationsDao(): LocationDao

    companion object {
        @Volatile
        private var instance: LocationDatabase? = null


        fun getInstance(context: Context): LocationDatabase {
            instance ?: synchronized(this) {
                instance ?:  Room.databaseBuilder(
                    context, LocationDatabase::class.java, "location_database"
                ).allowMainThreadQueries()
                    .build().also { instance = it }
            }
            return instance!!
        }

    }
}