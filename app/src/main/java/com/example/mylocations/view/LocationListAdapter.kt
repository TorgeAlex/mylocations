package com.example.mylocations.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mylocations.R
import com.example.mylocations.data.Location

/**
 * Created by Alex Torge on 1/25/19.
 */
class LocationListAdapter(items: List<Location>) :
    RecyclerView.Adapter<LocationListAdapter.CustomViewHolder>() {
    private var items: List<Location> = items

    fun setItems(items : List<Location>){
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        return CustomViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.location_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val location = items[holder.adapterPosition]
        holder.idTv?.text = location.id.toString()
        holder.labelTV?.text = location.label
        holder.latLonTv?.text = location.lat.toString()+", "+ location.lon.toString()
    }


    class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var idTv: TextView? = null
        var labelTV: TextView? = null
        var latLonTv: TextView? = null

        init {
            idTv = itemView.findViewById(R.id.magnitude_tv) as TextView
            labelTV = itemView.findViewById(R.id.location_tv) as TextView
            latLonTv = itemView.findViewById(R.id.date_tv) as TextView
        }

    }

}