package com.example.mylocations.view

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mylocations.LocationListViewModel
import com.example.mylocations.R
import com.example.mylocations.data.Location
import com.example.mylocations.utils.InjectorUtils

import kotlinx.android.synthetic.main.activity_location_list.*

class LocationListActivity : AppCompatActivity() {

    lateinit var viewModel: LocationListViewModel
    lateinit var recyclerView : RecyclerView
    var adapter: LocationListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_list)
        setSupportActionBar(toolbar)
        initUi()
        fab.setOnClickListener { view ->

            viewModel.addLocation(Location(0,"Work", 12.0,12.0))

            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    private fun initUi(){
        val factory = InjectorUtils.provideViewModelFactory(applicationContext)
        viewModel = ViewModelProviders.of(this,factory).get(LocationListViewModel::class.java)
        viewModel.getLocations().observe(this, Observer {
            if(adapter == null) {
                adapter = LocationListAdapter(it)
                recyclerView.adapter = adapter
            } else {
                adapter?.setItems(it)
            }
        })
        recyclerView = findViewById(R.id.location_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

}
