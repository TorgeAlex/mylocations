package com.example.mylocations

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mylocations.data.LocationRepository

/**
 * Created by Alex Torge on 1/25/19.
 */
@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val locationRepository
                       : LocationRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LocationListViewModel(locationRepository) as T
    }

}