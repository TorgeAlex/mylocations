package com.example.mylocations.utils

import android.content.Context
import com.example.mylocations.ViewModelFactory
import com.example.mylocations.data.LocationDatabase
import com.example.mylocations.data.LocationRepository

/**
 * Created by Alex Torge on 1/25/19.
 */
object InjectorUtils {

    fun provideViewModelFactory(context: Context) : ViewModelFactory {
        val locationRepository = LocationRepository(LocationDatabase.getInstance(context).locationsDao())
        return ViewModelFactory(locationRepository)
    }
}